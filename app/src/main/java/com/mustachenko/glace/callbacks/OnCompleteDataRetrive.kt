package com.mustachenko.glace.callbacks

interface OnCompleteDataRetrieve {
    fun retrieve(items: List<Any>)
}
