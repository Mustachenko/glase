package com.mustachenko.glace.callbacks

import androidx.recyclerview.widget.DiffUtil

import com.mustachenko.glace.data.Dessert

class DesertsDiffUtilCallback(private val oldList: List<Dessert>, private val newList: List<Dessert>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(i: Int, i1: Int): Boolean {
        return oldList[i].image.equals(newList[i1].image, ignoreCase = true)
    }

    override fun areContentsTheSame(i: Int, i1: Int): Boolean {
        return oldList[i].price.equals(newList[i1].price, ignoreCase = true)
                && oldList[i].name.equals(newList[i1].name, ignoreCase = true)
                && oldList[i].image.equals(newList[i1].image, ignoreCase = true)
    }
}
