package com.mustachenko.glace.data


class Dessert {
    var image = ""
    var name = ""
    var price = ""
        get() = field + "₴"
    var description = ""
}
