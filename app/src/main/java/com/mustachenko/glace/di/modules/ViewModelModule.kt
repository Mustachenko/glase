package com.mustachenko.glace.di.modules

import androidx.lifecycle.ViewModel
import com.mustachenko.glace.view_models.DessertViewModel

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(DessertViewModel::class)
    internal abstract fun bindViewModel(viewModel: ViewModel): ViewModel
}
