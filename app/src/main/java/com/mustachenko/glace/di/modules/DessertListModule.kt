package com.mustachenko.glace.di.modules

import androidx.recyclerview.widget.LinearLayoutManager
import com.mustachenko.glace.App
import com.mustachenko.glace.adapters.TastyRecyclerViewAdapter
import com.mustachenko.glace.model.DessertModel
import dagger.Module
import dagger.Provides
import java.util.*

@Module
class DessertListModule {
    @Provides
    internal fun tastyRecyclerViewAdapterProvider(): TastyRecyclerViewAdapter {
        return TastyRecyclerViewAdapter(ArrayList())
    }

    @Provides
    internal fun desertModelProvider(): DessertModel {
        return DessertModel()
    }

    @Provides
    internal fun linearLayoutManagerProvider(app: App): LinearLayoutManager {
        return LinearLayoutManager(app)
    }
}
