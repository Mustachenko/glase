package com.mustachenko.glace.di.modules

import com.mustachenko.glace.adapters.LastSeenRecyclerViewAdapter
import com.mustachenko.glace.adapters.NewsAdapter
import com.mustachenko.glace.model.MainModel
import dagger.Module
import dagger.Provides
import java.util.*


@Module
class MainModule {
    @Provides
    internal fun newsAdapterProvider(): NewsAdapter {
        return NewsAdapter()
    }

    @Provides
    internal fun lastSeenRecyclerViewAdapterProvider(): LastSeenRecyclerViewAdapter {
        return LastSeenRecyclerViewAdapter(ArrayList())
    }

    @Provides
    internal fun mainModelProvider(): MainModel {
        return MainModel()
    }
}