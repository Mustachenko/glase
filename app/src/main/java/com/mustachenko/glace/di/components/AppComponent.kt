package com.mustachenko.glace.di.components

import androidx.recyclerview.widget.LinearLayoutManager
import com.mustachenko.glace.App
import com.mustachenko.glace.adapters.LastSeenRecyclerViewAdapter
import com.mustachenko.glace.adapters.NewsAdapter
import com.mustachenko.glace.adapters.TastyRecyclerViewAdapter
import com.mustachenko.glace.di.modules.DessertListModule
import com.mustachenko.glace.di.modules.MainModule
import com.mustachenko.glace.di.modules.ViewModelModule
import com.mustachenko.glace.model.DessertModel
import com.mustachenko.glace.model.MainModel
import dagger.BindsInstance
import dagger.Component

@Component(modules = [ViewModelModule::class, DessertListModule::class, MainModule::class])
interface AppComponent {

    val tastyRecyclerViewAdapter: TastyRecyclerViewAdapter
    val linearLayoutManager: LinearLayoutManager
    val desertModel: DessertModel
    val mainModel: MainModel
    val newsAdapter: NewsAdapter
    val lastSeenRecyclerViewAdapter: LastSeenRecyclerViewAdapter


    fun inject(app: App)
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun withApplication(application: App): Builder

        fun build(): AppComponent
    }
}
