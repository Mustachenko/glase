package com.mustachenko.glace

import android.app.Application

import com.google.firebase.FirebaseApp
import com.mustachenko.glace.di.components.AppComponent
import com.mustachenko.glace.di.components.DaggerAppComponent

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)

        val _appComponent = DaggerAppComponent
                .builder().withApplication(this)
                .build()
        _appComponent.inject(this)
        appComponent = _appComponent
    }


    companion object {
        lateinit var appComponent: AppComponent
    }
}
