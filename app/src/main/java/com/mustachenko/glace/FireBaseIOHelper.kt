package com.mustachenko.glace

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.mustachenko.glace.data.Dessert
import com.mustachenko.glace.data.News
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import io.reactivex.Observable

object FireBaseIOHelper {

    fun setImageFromUri(imageUri: String, imageHeight: Int, imageWidth: Int, target: Target) {
        if (!imageUri.isEmpty()) {
            val auth = FirebaseAuth.getInstance()
            if (auth.currentUser == null) {
                auth.signInAnonymously()
            }
            val reference = FirebaseStorage.getInstance().getReferenceFromUrl(imageUri)
            reference.downloadUrl.addOnSuccessListener { uri ->
                Picasso.get().load(uri).centerInside().placeholder(R.drawable.component_place_holder)
                        .resize(imageWidth, imageHeight).into(target)
            }
        }
    }

    fun getNewsItems() : Observable<News> {
        return Observable.create { emitter ->
            val newsReferences = FirebaseDatabase.getInstance().getReference("news")
            newsReferences.addValueEventListener(object  : ValueEventListener {
                override fun onCancelled(e: DatabaseError) {
                    Log.e("FireBase", "onCancelled: ", e.toException())
                }

                override fun onDataChange(dataSnapShot: DataSnapshot) {
                    for (shot in dataSnapShot.children) {
                        val news = News()
                        news.information =  shot.child("info").value.toString()
                        news.newsImage = shot.child("image").value.toString()
                        news.newsTag = shot.child("tag").value.toString()
                        emitter.onNext(news)
                    }
                    emitter.onComplete()
                }

            })
        }
    }

    fun getCategoryItems(categoryName: String): Observable<Dessert> {
        return Observable.create { emitter ->
            if (categoryName == "ice-cream" || categoryName == "drinks") {
                val categoryReference = FirebaseDatabase.getInstance().getReference("menu/$categoryName")
                categoryReference.addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        for (dataSnapshot1 in dataSnapshot.children) {
                            val desert = Dessert()
                            desert.name = dataSnapshot1.key.toString()

                            val descChild = dataSnapshot1.child("description").value
                            val priceChild = dataSnapshot1.child("price").value
                            val imageChild = dataSnapshot1.child("image").value

                            if (priceChild != null) desert.price = dataSnapshot1.child("price").value!!.toString()

                            if (imageChild != null) desert.image = dataSnapshot1.child("image").value!!.toString()

                            if (descChild != null) desert.description = dataSnapshot1.child("description").value!!.toString()
                            emitter.onNext(desert)
                        }
                        emitter.onComplete()
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        Log.e("FireBase", "onCancelled: ", databaseError.toException())
                    }
                })
            } else {
                try {
                    throw NullPointerException("no such category exception")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }
}
