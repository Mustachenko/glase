package com.mustachenko.glace.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.mustachenko.glace.data.Dessert
import io.reactivex.Flowable
import retrofit2.http.DELETE

@Dao
interface DessertsDao {

    @Query("SELECT * FROM CachedDesserts")
    fun getAllFromCache() : Flowable<Dessert>

    @Insert(onConflict = REPLACE)
    fun addToCache(cached: CachedDesserts)

    @Query("SELECT COUNT(*)")
    fun getSize(): Int

    /**
     * maximum 6 items in cache. If more, then delete the oldest
     * **/
    @DELETE
    fun deleteOld(toDelete: CachedDesserts)

    @Query("SELECT MIN(id) FROM CachedDesserts")
    fun getMinId(): Int
}