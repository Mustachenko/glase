package com.mustachenko.glace.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CachedDesserts (@PrimaryKey(autoGenerate = true) var id: Int? = null,
                           @ColumnInfo(name = "name") var name: String = "",
                           @ColumnInfo(name = "description") var description: String = "",
                           @ColumnInfo(name = "price") var price: String = "",
                           @ColumnInfo(name = "imageURL") var image: String = "")
