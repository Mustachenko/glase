package com.mustachenko.glace.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [CachedDesserts::class], version = 1)
abstract class CacheIO : RoomDatabase() {

    abstract fun dessertDao() : DessertsDao

    companion object {
        private var INSTANCE : CacheIO? = null

        fun getInstance(context: Context) : CacheIO? {
            if (INSTANCE == null) {
                synchronized(CacheIO::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext, CacheIO::class.java, "glace.db").build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

    }
}