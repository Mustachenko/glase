package com.mustachenko.glace.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageButton
import android.widget.LinearLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.card.MaterialCardView
import com.mustachenko.glace.R
import com.mustachenko.glace.view_models.MainViewModel

class MainFragment : Fragment() {
    private var viewModel: MainViewModel? = null
    private var recipesButton: ImageButton? = null
    private var lastSeen: LinearLayout? = null
    private var lastSeenRecyclerView: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.landing_fragment, container, false) as CoordinatorLayout
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        val places = root.findViewById<MaterialCardView>(R.id.places)
        lastSeen = root.findViewById(R.id.last_seen)
        lastSeenRecyclerView = root.findViewById(R.id.last_seen_list)
        recipesButton = root.findViewById(R.id.more_recipes_info)
        val newsPager = root.findViewById<ViewPager>(R.id.news)

        newsPager.pageMargin = resources.getDimensionPixelSize(R.dimen.descriptionHeight)
        newsPager.adapter = viewModel!!.newsAdapter

        val recipesAnimation = AnimationUtils.loadAnimation(context, R.anim.recipes_button_animation)
        recipesAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                recipesButton?.visibility = View.INVISIBLE
                fragmentManager!!.beginTransaction().addToBackStack("recipes")
                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.container, DessertFragment()).commit()
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })

        recipesButton?.setOnClickListener { recipesButton?.startAnimation(recipesAnimation) }

        places.setOnClickListener { }

        return root
    }

    override fun onResume() {
        super.onResume()
        recipesButton!!.visibility = View.VISIBLE
        viewModel?.updateNews()
        viewModel?.updateLastSeen()
        if (!viewModel?.getLastSeenTasties()?.isEmpty()!!) {
            lastSeen?.visibility = View.GONE
        } else {
            lastSeen?.visibility = View.VISIBLE
            lastSeenRecyclerView?.adapter = viewModel?.lastSeenAdapter
        }
    }
}

