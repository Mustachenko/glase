package com.mustachenko.glace.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.mustachenko.glace.App
import com.mustachenko.glace.R
import com.mustachenko.glace.factories.DaggerViewModelFactory
import com.mustachenko.glace.view_models.DessertViewModel
import javax.inject.Inject


class DessertFragment : Fragment() {

    private var viewModelFactory: DaggerViewModelFactory? = null
    private var viewModel: DessertViewModel? = null

    @Inject
    internal fun setViewModelFactory(viewModelFactory: DaggerViewModelFactory) {
        this.viewModelFactory = viewModelFactory
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_dessert, container, false) as DrawerLayout
        // Inflate the layout for this fragment
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DessertViewModel::class.java)
        val recyclerView = root.findViewById<RecyclerView>(R.id.list)
        recyclerView.layoutManager = App.appComponent.linearLayoutManager
        recyclerView.adapter = viewModel!!.recyclerViewAdapter
        return root
    }

    override fun onResume() {
        super.onResume()
        viewModel!!.updateAdapterData()
    }
}
