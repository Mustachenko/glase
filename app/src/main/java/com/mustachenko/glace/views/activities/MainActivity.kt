package com.mustachenko.glace.views.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

import com.mustachenko.glace.R
import com.mustachenko.glace.views.fragments.MainFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity)
        supportFragmentManager.beginTransaction().replace(R.id.container, MainFragment()).commit()
    }
}
