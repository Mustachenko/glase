package com.mustachenko.glace.model

import android.content.Context
import android.util.Log
import com.mustachenko.glace.FireBaseIOHelper
import com.mustachenko.glace.callbacks.OnCompleteDataRetrieve
import com.mustachenko.glace.data.Dessert
import com.mustachenko.glace.room.CacheIO
import com.mustachenko.glace.room.CachedDesserts
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class DessertModel {

    fun getDesertsAsList(categoryName: String, retriever: OnCompleteDataRetrieve) {
        val deserts = ArrayList<Dessert>()
        FireBaseIOHelper.getCategoryItems(categoryName).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe({dessert ->   deserts.add(dessert)}, {e -> Log.e("FireBase", "onError: ", e) }, { retriever.retrieve(deserts)}
            ).dispose()
    }

    fun addTastyToCache(dessert: Dessert, context: Context) {
        val cache  = CacheIO.getInstance(context)
        val dao = cache!!.dessertDao()
        if (cache.dessertDao().getSize() > 6) {
            dao.deleteOld(CachedDesserts(id = dao.getMinId()))
        }
        val toCache = CachedDesserts(name = dessert.name, description = dessert.description, price = dessert.price, image = dessert.image)
        Completable.fromAction { cache.dessertDao().addToCache(toCache) }.subscribeOn(Schedulers.io()).subscribe()

    }
}
