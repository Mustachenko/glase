package com.mustachenko.glace.model

import android.content.Context
import android.util.Log
import com.mustachenko.glace.FireBaseIOHelper
import com.mustachenko.glace.callbacks.OnCompleteDataRetrieve
import com.mustachenko.glace.data.Dessert
import com.mustachenko.glace.data.News
import com.mustachenko.glace.room.CacheIO
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.ArrayList

class MainModel {

    fun getLastSeenTastiesAsList(context: Context, retriever: OnCompleteDataRetrieve) {
        val cacheIO = CacheIO.getInstance(context)
        val desserts  = ArrayList<Dessert>()
        cacheIO?.dessertDao()?.getAllFromCache()?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({dessert ->  desserts.add(dessert)},
                        { e -> Log.i(this::class.simpleName,"error during data retrieve from DB ${e?.message}")},
                        {retriever.retrieve(desserts)}
        )?.dispose()
    }

    fun getNewsAsList(retriever: OnCompleteDataRetrieve) {
        val newsList = ArrayList<News>()
        FireBaseIOHelper.getNewsItems().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe( {news -> newsList.add(news)},
                        {e ->    Log.e("FireBase", "onError: ", e)},
                        {retriever.retrieve(newsList)}).dispose()
    }

}
