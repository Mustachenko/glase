package com.mustachenko.glace.adapters

import android.graphics.*
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.palette.graphics.Palette
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.card.MaterialCardView
import com.mustachenko.glace.FireBaseIOHelper
import com.mustachenko.glace.R
import com.mustachenko.glace.data.News
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class NewsAdapter : PagerAdapter() {

    private var newsList = ArrayList<News>()

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val root = LayoutInflater.from(container.context).inflate(R.layout.news_layout, container, false) as LinearLayout
        val newsImage = root.findViewById<ImageView>(R.id.newsImage)
        val newsTypeIcon = root.findViewById<ImageView>(R.id.tag)
        val info = root.findViewById<TextView>(R.id.info)
        info.text = newsList[position].information

        when (newsList[position].newsTag) {
            "sale" -> {
            }
            "new place" -> {
            }
            "advertisement" -> {
            }
        }

        val imageSize = root.resources.getDimensionPixelSize(R.dimen.news_image_size)
        FireBaseIOHelper.setImageFromUri(newsList[position].newsImage, imageSize, imageSize, object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                Palette.from(bitmap).generate { palette ->
                    if (bitmap.getPixel(0, 0) == Color.TRANSPARENT) {
                        val radius = root.resources.getDimensionPixelSize(R.dimen.image_radius)
                        val color = palette?.getDominantColor(root.resources.getColor(R.color.colorPrimary))
                        val resultBitmap = Bitmap.createBitmap(imageSize, imageSize, bitmap.config)
                        val canvas = Canvas(resultBitmap)
                        canvas.drawColor(Color.TRANSPARENT)

                        val rect = RectF(0f, 0f, newsImage.width.toFloat(), newsImage.height.toFloat())
                        val clipPath = Path()
                        clipPath.addRoundRect(rect, radius.toFloat(), radius.toFloat(), Path.Direction.CW)
                        val paint = Paint()
                        paint.isDither = true
                        paint.color = color!!
                        paint.style = Paint.Style.FILL_AND_STROKE
                        paint.strokeJoin = Paint.Join.ROUND
                        paint.strokeCap = Paint.Cap.ROUND
                        paint.isAntiAlias = true
                        canvas.drawPath(clipPath, paint)

                        val centerX = (imageSize - bitmap.width shr 1).toFloat()
                        val centerY = (imageSize - bitmap.height shr 1).toFloat()
                        canvas.drawBitmap(bitmap, centerX, centerY, null)

                        newsImage.setImageBitmap(resultBitmap)
                    }
                }
            }

            override fun onBitmapFailed(e: Exception, errorDrawable: Drawable) {}

            override fun onPrepareLoad(placeHolderDrawable: Drawable) {}
        })

        val viewPager = container as ViewPager
        viewPager.addView(root, position)
        return root
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
        val viewPager = container as ViewPager
        val view = `object` as View
        viewPager.removeView(view)
    }

    override fun getCount(): Int {
        return newsList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    fun updateNews(news: List<News>) {
        newsList = news as ArrayList<News>
        notifyDataSetChanged()
    }
}
