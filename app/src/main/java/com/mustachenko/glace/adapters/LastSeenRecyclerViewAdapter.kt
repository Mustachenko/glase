package com.mustachenko.glace.adapters

import android.graphics.*
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mustachenko.glace.FireBaseIOHelper
import com.mustachenko.glace.R
import com.mustachenko.glace.callbacks.DesertsDiffUtilCallback
import com.mustachenko.glace.data.Dessert
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import javax.inject.Inject

class LastSeenRecyclerViewAdapter @Inject constructor(private var dessertsList: List<Dessert>) : RecyclerView.Adapter<LastSeenRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.last_seen_layout, parent, false) as LinearLayout)
    }

    override fun getItemCount(): Int {
        return dessertsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val root = holder.layout
        holder.image = holder.layout.findViewById(R.id.image)
        holder.name = holder.layout.findViewById(R.id.name)

        holder.name!!.text = dessertsList[position].name
        val imageSize = root.resources.getDimensionPixelSize(R.dimen.last_seen_tasty_image_size)
        FireBaseIOHelper.setImageFromUri(dessertsList[position].image, imageSize, imageSize, object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                Palette.from(bitmap).generate { palette ->
                    if (bitmap.getPixel(0, 0) == Color.TRANSPARENT) {
                        val radius = root.resources.getDimensionPixelSize(R.dimen.image_radius)
                        val color = palette?.getDominantColor(root.resources.getColor(R.color.colorPrimary))
                        val resultBitmap = Bitmap.createBitmap(imageSize, imageSize, bitmap.config)
                        val canvas = Canvas(resultBitmap)
                        canvas.drawColor(Color.TRANSPARENT)

                        val rect = RectF(0f, 0f, holder.image!!.width.toFloat(), holder.image!!.height.toFloat())
                        val clipPath = Path()
                        clipPath.addRoundRect(rect, radius.toFloat(), radius.toFloat(), Path.Direction.CW)
                        val paint = Paint()
                        paint.isDither = true
                        paint.color = color!!
                        paint.style = Paint.Style.FILL_AND_STROKE
                        paint.strokeJoin = Paint.Join.ROUND
                        paint.strokeCap = Paint.Cap.ROUND
                        paint.isAntiAlias = true
                        canvas.drawPath(clipPath, paint)

                        val centerX = (imageSize - bitmap.width shr 1).toFloat()
                        val centerY = (imageSize - bitmap.height shr 1).toFloat()
                        canvas.drawBitmap(bitmap, centerX, centerY, null)

                        holder.image!!.setImageBitmap(resultBitmap)
                    }
                }
            }

            override fun onBitmapFailed(e: Exception, errorDrawable: Drawable) {}

            override fun onPrepareLoad(placeHolderDrawable: Drawable) {}
        })

    }

    fun updateDeserts(deserts: List<Dessert>) {
        val desertsDiffUtilCallback = DesertsDiffUtilCallback(this.dessertsList, deserts)
        val diffResult = DiffUtil.calculateDiff(desertsDiffUtilCallback)
        diffResult.dispatchUpdatesTo(this)
        this.dessertsList = deserts
    }

    class ViewHolder(var layout: LinearLayout) : RecyclerView.ViewHolder(layout) {
        internal var image: ImageView? = null
        internal var name: TextView? = null
    }
}