package com.mustachenko.glace.adapters

import android.graphics.*
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mustachenko.glace.FireBaseIOHelper
import com.mustachenko.glace.R
import com.mustachenko.glace.callbacks.DesertsDiffUtilCallback
import com.mustachenko.glace.data.Dessert
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import javax.inject.Inject

class TastyRecyclerViewAdapter @Inject
constructor(private var desserts: List<Dessert>) : RecyclerView.Adapter<TastyRecyclerViewAdapter.ExpandableCardViewHolder>() {


    private var onExpand: ExpandableCardViewHolder.OnExpand? = null

    fun setOnExpandAction(_onExpand: ExpandableCardViewHolder.OnExpand) {
        onExpand = _onExpand
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ExpandableCardViewHolder {
        val layout = LayoutInflater.from(viewGroup.context).inflate(R.layout.tasty_item_layout, viewGroup, false) as LinearLayout
        return ExpandableCardViewHolder(layout)
    }

    override fun onBindViewHolder(viewHolder: ExpandableCardViewHolder, i: Int) {
        val root = viewHolder.root

        viewHolder.image = root.findViewById(R.id.yummy_picture)
        viewHolder.name = root.findViewById(R.id.name)
        viewHolder.price = root.findViewById(R.id.price)
        viewHolder.textLayout = root.findViewById(R.id.text_layout)

        if (i % 2 == 0) {
            root.setBackgroundResource(R.drawable.list_second_gradient)
        } else
            root.setBackgroundResource(R.drawable.list_first_gradient)

        viewHolder.description = root.findViewById(R.id.description)
        viewHolder.descLayout = root.findViewById(R.id.description_card)
        viewHolder.description!!.text = desserts[i].description

        root.setOnClickListener {
            if (viewHolder.descLayout!!.visibility == View.GONE) {
                onExpand!!.run(desserts[i])
                ExpandableCardViewHolder.expand(viewHolder.descLayout!!)

            } else
                ExpandableCardViewHolder.collapse(viewHolder.descLayout!!)
        }

        viewHolder.name!!.text = desserts[i].name
        viewHolder.price!!.text = desserts[i].price

        val imageHeight = root.resources.getDimensionPixelSize(R.dimen.image_height)
        val imageWidth = root.resources.displayMetrics.widthPixels - root.resources.getDimensionPixelSize(R.dimen.card_margin) * 2
        FireBaseIOHelper.setImageFromUri(desserts[i].image, imageHeight, imageWidth, object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                Palette.from(bitmap).generate { palette ->
                    val radius = root.resources.getDimensionPixelSize(R.dimen.image_radius)
                    val color = palette?.getDominantColor(root.resources.getColor(R.color.colorPrimary))
                    val shadowSize = root.resources.getDimensionPixelSize(R.dimen.shadow_size)
                    val bitmapHeight = (imageHeight + shadowSize * 3.5f).toInt()
                    val shadowRadius = root.resources.getDimensionPixelSize(R.dimen.shadow_radius)
                    val resultBitmap = Bitmap.createBitmap(imageWidth, bitmapHeight, bitmap.config)
                    val canvas = Canvas(resultBitmap)
                    canvas.drawColor(Color.TRANSPARENT)
                    val rect = RectF(0f, 0f, viewHolder.image!!.width.toFloat(), viewHolder.image!!.height.toFloat())
                    val clipPath = Path()
                    clipPath.addRoundRect(rect, radius.toFloat(), radius.toFloat(), Path.Direction.CW)
                    val paint = Paint()
                    paint.isDither = true

                    if (bitmap.getPixel(0, 0) == Color.TRANSPARENT)
                        paint.color = color!!
                    else
                        paint.color = bitmap.getPixel(0, 0)

                    paint.style = Paint.Style.FILL_AND_STROKE
                    paint.strokeJoin = Paint.Join.ROUND
                    paint.strokeCap = Paint.Cap.ROUND
                    paint.isAntiAlias = true
                    paint.setShadowLayer(shadowRadius.toFloat(), 0f, shadowSize.toFloat(), root.resources.getColor(R.color.shadowColor))
                    canvas.drawPath(clipPath, paint)

                    val centerX = (imageWidth - bitmap.width shr 1).toFloat()
                    val centerY = (imageHeight - bitmap.height shr 1).toFloat()
                    canvas.drawBitmap(bitmap, centerX, centerY, null)

                    viewHolder.image!!.setImageBitmap(resultBitmap)
                }
            }

            override fun onBitmapFailed(e: Exception, errorDrawable: Drawable) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable) {}
        })
    }

    override fun getItemCount(): Int {
        return desserts.size
    }

    fun updateDeserts(deserts: List<Dessert>) {
        val desertsDiffUtilCallback = DesertsDiffUtilCallback(this.desserts, deserts)
        val diffResult = DiffUtil.calculateDiff(desertsDiffUtilCallback)
        diffResult.dispatchUpdatesTo(this)
        this.desserts = deserts
    }

    class ExpandableCardViewHolder(var root: LinearLayout) : RecyclerView.ViewHolder(root) {
        internal var image: ImageView? = null
        internal var name: TextView? = null
        internal var price: TextView? = null
        internal var description: TextView? = null
        internal var descLayout: LinearLayout? = null
        internal var textLayout: RelativeLayout? = null

        interface OnExpand {
            fun run(dessert: Dessert)
        }

        companion object {

            fun expand(v: View) {
                v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                val targetHeight = v.measuredHeight

                // Older versions of android (pre API 21) cancel animations for views with a height of 0.
                v.layoutParams.height = 1
                v.visibility = View.VISIBLE
                val a = object : Animation() {
                    override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                        v.layoutParams.height = if (interpolatedTime == 1f)
                            ViewGroup.LayoutParams.WRAP_CONTENT
                        else
                            (targetHeight * interpolatedTime).toInt()
                        v.requestLayout()
                    }

                    override fun willChangeBounds(): Boolean {
                        return true
                    }
                }

                // 1dp/ms
                a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
                v.startAnimation(a)
            }

            fun collapse(v: View) {
                val initialHeight = v.measuredHeight

                val a = object : Animation() {
                    override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                        if (interpolatedTime == 1f) {
                            v.visibility = View.GONE
                        } else {
                            v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                            v.requestLayout()
                        }
                    }

                    override fun willChangeBounds(): Boolean {
                        return true
                    }
                }

                // 1dp/ms
                a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
                v.startAnimation(a)
            }
        }
    }
}
