package com.mustachenko.glace.view_models

import android.app.Application
import androidx.lifecycle.AndroidViewModel

import com.mustachenko.glace.App
import com.mustachenko.glace.adapters.TastyRecyclerViewAdapter
import com.mustachenko.glace.callbacks.OnCompleteDataRetrieve
import com.mustachenko.glace.data.Dessert
import com.mustachenko.glace.model.DessertModel

import javax.inject.Inject

class DessertViewModel @Inject
constructor(application: Application) : AndroidViewModel(application) {
    val recyclerViewAdapter: TastyRecyclerViewAdapter = App.appComponent.tastyRecyclerViewAdapter

    init {
        recyclerViewAdapter.setOnExpandAction(object : TastyRecyclerViewAdapter.ExpandableCardViewHolder.OnExpand {
            override fun run(dessert: Dessert) {
                desertModel.addTastyToCache(dessert, getApplication())
            }
        })
    }
    private val desertModel: DessertModel = App.appComponent.desertModel


    fun updateAdapterData(categoryName: String = "ice-cream") {
        desertModel.getDesertsAsList(categoryName, object : OnCompleteDataRetrieve {
            override fun retrieve(items: List<Any>) {
                recyclerViewAdapter.updateDeserts(items as List<Dessert>)
            }
        })
    }
}
