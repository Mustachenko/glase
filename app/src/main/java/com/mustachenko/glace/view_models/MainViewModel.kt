package com.mustachenko.glace.view_models

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.mustachenko.glace.App
import com.mustachenko.glace.adapters.LastSeenRecyclerViewAdapter
import com.mustachenko.glace.adapters.NewsAdapter
import com.mustachenko.glace.callbacks.OnCompleteDataRetrieve
import com.mustachenko.glace.data.Dessert
import com.mustachenko.glace.data.News
import com.mustachenko.glace.model.MainModel
import javax.inject.Inject


class MainViewModel @Inject constructor(application: Application) : AndroidViewModel(application) {
    @Inject
    var newsAdapter: NewsAdapter? = null
    @Inject
    var lastSeenAdapter: LastSeenRecyclerViewAdapter? = null
    private val model: MainModel = App.appComponent.mainModel
    private var lastSeenTasties : List<Dessert>? = null

    fun getLastSeenTasties(): List<Dessert>? {
        return lastSeenTasties
    }

    fun updateNews() {
        model.getNewsAsList(object : OnCompleteDataRetrieve {
            override fun retrieve(items: List<Any>) {
                newsAdapter?.updateNews(items as List<News>)
            }
        })
    }

    fun updateLastSeen() {
        model.getLastSeenTastiesAsList(getApplication(), object : OnCompleteDataRetrieve {
            override fun retrieve(items: List<Any>) {
                lastSeenTasties = items as List<Dessert>
                lastSeenAdapter?.updateDeserts(lastSeenTasties!!)
            }
        })
    }

}