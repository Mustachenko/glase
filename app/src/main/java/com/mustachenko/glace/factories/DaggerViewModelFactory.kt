package com.mustachenko.glace.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Singleton
class DaggerViewModelFactory @Inject
constructor(private val creators: Map<Class<out ViewModel>, Provider<ViewModel>>) : ViewModelProvider.Factory {

    private fun setChecker(creator: Provider<out ViewModel>?, modelClass: Class<out ViewModel>): Provider<out ViewModel>? {
        var resultCreator: Provider<out ViewModel>? = null
        if (creator == null) {
            for ((key, value) in creators) {
                if (modelClass.isAssignableFrom(key)) {
                    resultCreator = value
                }
            }
        }
        return resultCreator
    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        var creator: Provider<out ViewModel>? = creators[modelClass]
        val creatorValue = setChecker(creator, modelClass)
        if (creatorValue != null) {
            creator = creatorValue
        }
        if (creator == null) {
            throw IllegalArgumentException("unknown model class $modelClass")
        }
        try {
            return creator.get() as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }
}